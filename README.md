# points2surface

This program converts a 3D point cloud into a 3D surface.

Surface reconstruction methods used by the program:

- Convex Hulls
- Optimal Bounding Box
- Poisson Surface Reconstruction
- Scale-Space Surface Reconstruction
- Advancing Front Surface Reconstruction
- Polygonal Surface Reconstruction
