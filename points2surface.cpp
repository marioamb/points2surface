/* @(#)points2surface.cpp
 *
 * Copyright (C) 2018 Mario Ambrogetti
 * Author: Mario Ambrogetti
 *
 * points2surface V1.4
 *
 * First version: Sun, 19 Aug 2018 13:32:08 +0200
 * Last version:  Fri, 15 Nov 2024 00:53:18 +0100
 *
 * This program converts a point cloud into a 3D surface.
 * The algorithms used are those present in the CGAL library:
 *
 *   https://www.cgal.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstring>
#include <exception>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/si/base.hpp>
#include <boost/version.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Timer.h>
#include <CGAL/Point_with_normal_3.h>
#include <CGAL/pca_estimate_normals.h>
#include <CGAL/jet_estimate_normals.h>
#include <CGAL/vcm_estimate_normals.h>
#include <CGAL/mst_orient_normals.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/optimal_bounding_box.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/compute_average_spacing.h>
#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Poisson_reconstruction_function.h>
#include <CGAL/Scale_space_surface_reconstruction_3.h>
#include <CGAL/Scale_space_reconstruction_3/Advancing_front_mesher.h>
#include <CGAL/Scale_space_reconstruction_3/Jet_smoother.h>
#include <CGAL/Shape_detection.h>
#include <CGAL/structure_point_set.h>
#include <CGAL/IO/facets_in_complex_2_to_triangle_mesh.h>
#include <CGAL/IO/read_points.h>
#include <CGAL/IO/read_ply_points.h>
#include <CGAL/IO/write_points.h>
#include <CGAL/IO/write_ply_points.h>
#include <CGAL/boost/graph/IO/OBJ.h>
#include <CGAL/boost/graph/IO/STL.h>
#include <CGAL/Polygon_mesh_processing/IO/polygon_mesh_io.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <CGAL/Shape_detection/Efficient_RANSAC.h>
#include <CGAL/Polygonal_surface_reconstruction.h>

#ifdef MYPROJECT_NAME
#define PROJECT STR(MYPROJECT_NAME)
#else
#define PROJECT "unknow"
#endif

#ifdef CMAKE_PROJECT_VERSION
#define VERSION STR(CMAKE_PROJECT_VERSION)
#else
#define VERSION "0.0"
#endif

// #define CFG_DUMP

#define OPT_HELP "help"
#define OPT_HELP_SHORT "h"

#define OPT_VERSION "version"
#define OPT_VERSION_SHORT "v"

#define OPT_VERBOSE "verbose"
#define OPT_VERBOSE_SHORT "V"

#define OPT_ORIENT "orient"
#define OPT_ORIENT_SHORT "O"

#define OPT_ESTIMATE "estimate"
#define OPT_ESTIMATE_SHORT "e"
#define OPT_ESTIMATE_NONE "none"
#define OPT_ESTIMATE_PLANE "plane"
#define OPT_ESTIMATE_QUADRIC "quadric"
#define OPT_ESTIMATE_VCM "vcm"
#define OPT_ESTIMATE_DEFAULT OPT_ESTIMATE_NONE

#define OPT_RECONSTRUCTION "reconstruction"
#define OPT_RECONSTRUCTION_SHORT "r"
#define OPT_RECONSTRUCTION_NONE "none"
#define OPT_RECONSTRUCTION_CONVEX_HULL "convex-hull"
#define OPT_RECONSTRUCTION_OPTIMAL_BOUNDING_BOX "optimal-bounding-box"
#define OPT_RECONSTRUCTION_POISSON "poisson"
#define OPT_RECONSTRUCTION_SCALE_SPACE "scale-space"
#define OPT_RECONSTRUCTION_FRONT_SURFACE "front-surface"
#define OPT_RECONSTRUCTION_POLYGONAL_SURFACE_RECONSTRUCTION \
    "polygonal-surface-reconstruction"
#define OPT_RECONSTRUCTION_DEFAULT OPT_RECONSTRUCTION_NONE

#define OPT_NEIGHBORS "neighbors"
#define OPT_NEIGHBORS_SHORT "n"
#define OPT_NEIGHBORS_DEFAULT 18

#define OPT_NEIGHBORS_MST "neighbors-mst"
#define OPT_NEIGHBORS_MST_SHORT "k"
#define OPT_NEIGHBORS_MST_DEFAULT 18

#define OPT_NEIGHBORS_AVG "neighbors-avg"
#define OPT_NEIGHBORS_AVG_SHORT "K"
#define OPT_NEIGHBORS_AVG_DEFAULT 6

#define OPT_OFFSET "offset"
#define OPT_OFFSET_SHORT "o"
#define OPT_OFFSET_DEFAULT 0.1

#define OPT_CONVOLUTION "convolution"
#define OPT_CONVOLUTION_SHORT "c"
#define OPT_CONVOLUTION_DEFAULT 0.0

#define OPT_DISTANCE "distance"
#define OPT_DISTANCE_SHORT "d"
#define OPT_DISTANCE_DEFAULT 0.375

#define OPT_ANGLE "angle"
#define OPT_ANGLE_SHORT "a"
#define OPT_ANGLE_DEFAULT 20

#define OPT_TRIANGLE "triangle"
#define OPT_TRIANGLE_SHORT "t"
#define OPT_TRIANGLE_DEFAULT 30

#define OPT_SMOOTHER "smoother"
#define OPT_SMOOTHER_SHORT "s"
#define OPT_SMOOTHER_DEFAULT 4

#define OPT_LENGTH "length"
#define OPT_LENGTH_SHORT "l"
#define OPT_LENGTH_DEFAULT 0.5

#define OPT_EPSILON "epsilon"
#define OPT_EPSILON_SHORT "E"
#define OPT_EPSILON_DEFAULT 0.002

#define OPT_CLUSTER "cluster"
#define OPT_CLUSTER_SHORT "U"
#define OPT_CLUSTER_DEFAULT 0.02

#define OPT_POINTS "points"
#define OPT_POINTS_SHORT "P"
#define OPT_POINTS_DEFAULT 100

#define OPT_PROBABILITY "probability"
#define OPT_PROBABILITY_SHORT "B"
#define OPT_PROBABILITY_DEFAULT 0.05

#define OPT_THRESHOLD "threshold"
#define OPT_THRESHOLD_SHORT "T"
#define OPT_THRESHOLD_DEFAULT 0.7

#define OPT_BINARY "binary"
#define OPT_BINARY_SHORT "b"
#define OPT_BINARY_DEFAULT false

#define OPT_CONFIG "config"
#define OPT_CONFIG_SHORT "C"

#define OPT_INPUT_FILE  "input-file"
#define OPT_OUTPUT_FILE "output-file"

#define EXT_PLY ".ply"
#define EXT_XYZ ".xyz"
#define EXT_PWN ".pwn"
#define EXT_OFF ".off"
#define EXT_OBJ ".obj"
#define EXT_STL ".stl"
#define EXT_TS  ".ts"

#ifdef CGAL_LINKED_WITH_LASLIB
#define EXT_LAS ".las"
#endif

#define _STR(x) #x
#define STR(x) _STR(x)

#define THROW_ERROR(MESSAGE) throw std::runtime_error(MESSAGE)

#define THROW_SYSTEM_ERROR(MESSAGE, FILENAME)                              \
    do {                                                                   \
        std::stringstream msg;                                             \
        msg << MESSAGE << bio::quoted(FILENAME.string());                  \
        throw std::system_error(errno, std::system_category(), msg.str()); \
    } while (0)

namespace bpo = boost::program_options;
namespace bfs = boost::filesystem;
namespace bun = boost::units;
namespace bio = boost::io;

// Kernel
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;

// Simple geometric types
typedef Kernel::FT FT;
typedef Kernel::Point_3 Point;
typedef Kernel::Vector_3 Normal;
typedef Kernel::Sphere_3 Sphere;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::HalfedgeDS HalfedgeDS;

// Point with normal vector
typedef CGAL::Point_with_normal_3<Kernel> Point_with_normal;
typedef std::vector<Point_with_normal> Point_vec;

// Maps point and normal
typedef CGAL::Identity_property_map<Point_with_normal> Point_map;
typedef CGAL::Normal_of_point_with_normal_map<Kernel> Normal_map;

// Point with normal and plane index
typedef boost::tuple<Point, Normal, int> Point_with_normal_index;
typedef std::vector<Point_with_normal_index> Point_index_vec;
typedef CGAL::Shape_detection::Efficient_RANSAC_traits<Kernel, Point_vec, Point_map, Normal_map> Traits_index;
typedef CGAL::Shape_detection::Point_to_shape_index_map<Traits_index> Point_to_shape_index_map;
typedef CGAL::Nth_of_tuple_property_map<0, Point_with_normal_index> Point_index_map;
typedef CGAL::Nth_of_tuple_property_map<1, Point_with_normal_index> Normal_index_map;
typedef CGAL::Nth_of_tuple_property_map<2, Point_with_normal_index> Plane_index_map;

// Polygonal surface reconstruction
typedef CGAL::Polygonal_surface_reconstruction<Kernel> Polygonal_surface_reconstruction;

// Surface mesh
typedef CGAL::Surface_mesh<Point> Surface_mesh;

// Poisson
typedef CGAL::Poisson_reconstruction_function<Kernel> Poisson_reconstruction_function;
typedef CGAL::Implicit_surface_3<Kernel, Poisson_reconstruction_function> Surface_3;
typedef CGAL::Surface_mesh_default_triangulation_3 STr;
typedef CGAL::Surface_mesh_complex_2_in_triangulation_3<STr> C2t3;

// Scale_space
typedef CGAL::Scale_space_surface_reconstruction_3<Kernel> Reconstruction;
typedef CGAL::Scale_space_reconstruction_3::Advancing_front_mesher<Kernel> Mesher;
typedef CGAL::Scale_space_reconstruction_3::Jet_smoother<Kernel> Smoother;

// Efficient RANSAC types
typedef CGAL::Shape_detection::Efficient_RANSAC_traits
    <Kernel, Point_vec, Point_map, Normal_map> Traits;
typedef CGAL::Shape_detection::Efficient_RANSAC<Traits> Efficient_ransac;
typedef CGAL::Shape_detection::Plane<Traits> Plane;

// Point set structuring type
typedef CGAL::Point_set_with_structure<Kernel> Structure;

// Advancing front types
typedef CGAL::Advancing_front_surface_reconstruction_vertex_base_3<Kernel> LVb;
typedef CGAL::Advancing_front_surface_reconstruction_cell_base_3<Kernel> LCb;
typedef CGAL::Triangulation_data_structure_3<LVb, LCb> Tds;
typedef CGAL::Delaunay_triangulation_3<Kernel, Tds> Triangulation_3;
typedef Triangulation_3::Vertex_handle Vertex_handle;
typedef CGAL::cpp11::array<std::size_t, 3> Facet;

#ifdef CGAL_USE_SCIP
#include <CGAL/SCIP_mixed_integer_program_traits.h>
typedef CGAL::SCIP_mixed_integer_program_traits<double> MIP_Solver;
#elif defined(CGAL_USE_GLPK)
#include <CGAL/GLPK_mixed_integer_program_traits.h>
typedef CGAL::GLPK_mixed_integer_program_traits<double> MIP_Solver;
#endif

/// Byte unit typedefs
struct byte_base_unit: bun::base_unit<byte_base_unit, bun::dimensionless_type, 1> {
    static constexpr const char *name()
    {
        return "byte";
    }
    static constexpr const char *symbol()
    {
        return "B";
    }
};

// Polyhedron incremental builder
template<class HDS>
class BuilderPoints: public CGAL::Modifier_base<HDS>
{
  private:

    Point_vec &m_points;

  public:

    BuilderPoints(Point_vec &points)
        : m_points(points)
    {
    }

    void operator()(HDS &hds)
    {
        // create a incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

        B.begin_surface(m_points.size(), 0);

        // add vertices
        BOOST_FOREACH(const Point_with_normal &pnt, m_points) {

            B.add_vertex(pnt.position());
        }

        // finish up the surface
        B.end_surface();
    }
};

// Polyhedron incremental builder
template<class HDS>
class BuilderPoisson: public CGAL::Modifier_base<HDS>
{
  private:

    Reconstruction &m_reconstruct;

  public:
    BuilderPoisson(Reconstruction &reconstruct)
        : m_reconstruct(reconstruct)
    {
    }

    void operator()(HDS &hds)
    {
        // create a incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

        // start surface
        B.begin_surface(m_reconstruct.number_of_points(),
                        m_reconstruct.number_of_facets());

        // add vertices
        for (Reconstruction::Point_const_iterator pnt =
                 m_reconstruct.points_begin();
             pnt != m_reconstruct.points_end(); ++pnt) {

            B.add_vertex(*pnt);
        }

        // add triangles
        for (Reconstruction::Facet_const_iterator fct =
                 m_reconstruct.facets_begin();
             fct != m_reconstruct.facets_end(); ++fct) {

            // start triangle
            B.begin_facet();

            B.add_vertex_to_facet((*fct)[0]);
            B.add_vertex_to_facet((*fct)[1]);
            B.add_vertex_to_facet((*fct)[2]);

            // finish triangle
            B.end_facet();
        }

        // finish surface
        B.end_surface();
    }
};

// Specialized priority functor that favor structure coherence
template <typename Structure> struct PriorityStructureCoherence {
    FT bound;
    Structure &structure;

    PriorityStructureCoherence(Structure &structure, FT bound)
        : structure(structure), bound(bound)
    {
    }

    template <typename AdvancingFront, typename Cell_handle>
    double operator()(AdvancingFront &adv, Cell_handle &c,
                      const std::size_t &index) const
    {
        // If perimeter > bound, return infinity so that facet is not used
        if (bound != 0) {
            FT d = std::sqrt(
                squared_distance(c->vertex((index + 1) % 4)->point(),
                                 c->vertex((index + 2) % 4)->point()));

            if (d > bound)
                return adv.infinity();

            d += std::sqrt(
                squared_distance(c->vertex((index + 2) % 4)->point(),
                                 c->vertex((index + 3) % 4)->point()));

            if (d > bound)
                return adv.infinity();

            d += std::sqrt(
                squared_distance(c->vertex((index + 1) % 4)->point(),
                                 c->vertex((index + 3) % 4)->point()));

            if (d > bound)
                return adv.infinity();
        }

        Facet f = { { c->vertex((index + 1) % 4)->info(),
                      c->vertex((index + 2) % 4)->info(),
                      c->vertex((index + 3) % 4)->info() } };

        // facet_coherence takes values between -1 and 3, 3 being the most
        // coherent and -1 being incoherent. Smaller weight means higher
        // priority.

        const FT weight = 100. * (5. - structure.facet_coherence(f));

        return weight * adv.smallest_radius_delaunay_sphere(c, index);
    }
};

typedef CGAL::Advancing_front_surface_reconstruction<Triangulation_3,
    PriorityStructureCoherence<Structure> > ReconstructionAFS;

// Polyhedron incremental builder
template<class HDS>
class BuilderAFS: public CGAL::Modifier_base<HDS>
{
  private:

    Structure &m_structure;
    ReconstructionAFS &m_reconstruct;

  public:

    BuilderAFS(Structure &structure, ReconstructionAFS &reconstruct):
        m_structure(structure), m_reconstruct(reconstruct)
    {}

    void operator()(HDS &hds)
    {
        // create a incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

        // start surface
        B.begin_surface(
            m_structure.size(),
            m_reconstruct.number_of_facets());

        // add vertices
        for (std::size_t idx = 0; idx < m_structure.size(); ++idx) {
            B.add_vertex(m_structure[idx].first);
        }

        const ReconstructionAFS::TDS_2 &tds =
            m_reconstruct.triangulation_data_structure_2();

        // add triangles
        for (ReconstructionAFS::TDS_2::Face_iterator fit = tds.faces_begin();
             fit != tds.faces_end(); ++fit) {

            if (fit->is_on_surface()) {
                // start triangle
                B.begin_facet();

                B.add_vertex_to_facet(fit->vertex(0)->vertex_3()->id());
                B.add_vertex_to_facet(fit->vertex(1)->vertex_3()->id());
                B.add_vertex_to_facet(fit->vertex(2)->vertex_3()->id());

                // finish triangle
                B.end_facet();
            }
        }

        // finish surface
        B.end_surface();
    }
};

int main(int argc, char **argv)
{
    unsigned int opt_neighbors = OPT_NEIGHBORS_DEFAULT;
    unsigned int opt_neighbors_mst = OPT_NEIGHBORS_MST_DEFAULT;
    unsigned int opt_neighbors_avg = OPT_NEIGHBORS_AVG_DEFAULT;

    std::size_t opt_smoother = OPT_SMOOTHER_DEFAULT;

    bool opt_binary = OPT_BINARY_DEFAULT;

    FT opt_offset_radius = OPT_OFFSET_DEFAULT;
    FT opt_convolution_radius = OPT_CONVOLUTION_DEFAULT;
    FT opt_distance = OPT_DISTANCE_DEFAULT;
    FT opt_angle = OPT_ANGLE_DEFAULT;
    FT opt_triangle = OPT_TRIANGLE_DEFAULT;
    FT opt_length = OPT_LENGTH_DEFAULT;
    FT opt_epsilon = OPT_EPSILON_DEFAULT;
    FT opt_cluster = OPT_CLUSTER_DEFAULT;
    FT opt_points = OPT_POINTS_DEFAULT;
    FT opt_probability = OPT_PROBABILITY_DEFAULT;
    FT opt_threshold = OPT_THRESHOLD_DEFAULT;

    std::string opt_estimate = OPT_ESTIMATE_DEFAULT;
    std::string opt_reconstruction = OPT_RECONSTRUCTION_DEFAULT;

    bool task_estimate_plane = false;
    bool task_estimate_quadric = false;
    bool task_estimate_vcm = false;

    bool task_convex_hull = false;
    bool task_optimal_bounding_box = false;
    bool task_poisson = false;
    bool task_scale_space = false;
    bool task_front_surface = false;
    bool task_polygonal_surface_reconstruction = false;

    bool task_read_xyz = false;
    bool task_read_off = false;
    bool task_read_ply = false;

    bool task_write_xyz = false;
    bool task_write_off = false;
    bool task_write_ply = false;
    bool task_write_obj = false;
    bool task_write_stl = false;
    bool task_write_ts = false;

#ifdef CGAL_LINKED_WITH_LASLIB
    bool task_read_las = false;
    bool task_write_las = false;
#endif

    bool write_file = false;

    // timer
    CGAL::Timer timer;

    // store points
    Point_vec points;

    // output polyhedron
    Polyhedron output_mesh;

    try {
        // parse command line

        bpo::options_description desc{
            PROJECT " V" VERSION " [options] input-file output-file\n\n"
            "Input formats:  XYZ,PWN,OFF,PLY,(LAS)"
            "\n"
            "Output formats: XYZ,PWN,OFF,PLY,OBJ,STL,TS,(LAS)"
            "\n\n"
            "Options"
        };

        desc.add_options()
        (OPT_HELP "," OPT_HELP_SHORT,
         "print help message")

        (OPT_VERSION "," OPT_VERSION_SHORT,
         "print version string")

        (OPT_VERBOSE "," OPT_VERBOSE_SHORT,
         "enable verbosity")

        (OPT_ORIENT "," OPT_ORIENT_SHORT,
         "enable normal orientation MST")

        (OPT_ESTIMATE "," OPT_ESTIMATE_SHORT,
         bpo::value<std::string>(&opt_estimate)->
         default_value(OPT_ESTIMATE_DEFAULT),
         "Estimates normals direction ("
         OPT_ESTIMATE_NONE ","
         OPT_ESTIMATE_PLANE ","
         OPT_ESTIMATE_QUADRIC ","
         OPT_ESTIMATE_VCM ")")

        (OPT_RECONSTRUCTION "," OPT_RECONSTRUCTION_SHORT,
         bpo::value<std::string>(&opt_reconstruction)->
         default_value(OPT_RECONSTRUCTION_DEFAULT),
         "Surface reconstruction method ("
         OPT_RECONSTRUCTION_NONE ","
         OPT_RECONSTRUCTION_CONVEX_HULL ","
         OPT_RECONSTRUCTION_OPTIMAL_BOUNDING_BOX ","
         OPT_RECONSTRUCTION_POISSON ","
         OPT_RECONSTRUCTION_SCALE_SPACE ","
         OPT_RECONSTRUCTION_FRONT_SURFACE ","
         OPT_RECONSTRUCTION_POLYGONAL_SURFACE_RECONSTRUCTION ")")

        (OPT_NEIGHBORS "," OPT_NEIGHBORS_SHORT,
         bpo::value<unsigned int>(&opt_neighbors)->
         default_value(OPT_NEIGHBORS_DEFAULT),
         "number of nearest neighbors around each point ("
         OPT_ESTIMATE_PLANE ","
         OPT_ESTIMATE_QUADRIC ")")

        (OPT_NEIGHBORS_MST "," OPT_NEIGHBORS_MST_SHORT,
         bpo::value<unsigned int>(&opt_neighbors_mst)->
         default_value(OPT_NEIGHBORS_MST_DEFAULT),
         "number of nearest neighbors around each point ("
         OPT_ORIENT ")")

        (OPT_NEIGHBORS_AVG "," OPT_NEIGHBORS_AVG_SHORT,
         bpo::value<unsigned int>(&opt_neighbors_avg)->
         default_value(OPT_NEIGHBORS_AVG_DEFAULT),
         "number of nearest neighbors around each point ("
         OPT_RECONSTRUCTION_POISSON ")")

        (OPT_OFFSET "," OPT_OFFSET_SHORT,
         bpo::value<FT>(&opt_offset_radius)->
         default_value(OPT_OFFSET_DEFAULT, STR(OPT_OFFSET_DEFAULT)),
         "offset radius (" OPT_ESTIMATE_VCM ")")

        (OPT_CONVOLUTION "," OPT_CONVOLUTION_SHORT,
         bpo::value<FT>(&opt_convolution_radius)->
         default_value(OPT_CONVOLUTION_DEFAULT),
         "convolution radius (" OPT_ESTIMATE_VCM ")")

        (OPT_DISTANCE "," OPT_DISTANCE_SHORT,
         bpo::value<FT>(&opt_distance)->
         default_value(OPT_DISTANCE_DEFAULT, STR(OPT_DISTANCE_DEFAULT)),
         "Surface approximation distance error ("
         OPT_RECONSTRUCTION_POISSON ")")

        (OPT_ANGLE "," OPT_ANGLE_SHORT,
         bpo::value<FT>(&opt_angle)->
         default_value(OPT_ANGLE_DEFAULT),
         "Min triangle angle in degrees ("
         OPT_RECONSTRUCTION_POISSON ")")

        (OPT_TRIANGLE "," OPT_TRIANGLE_SHORT,
         bpo::value<FT>(&opt_triangle)->
         default_value(OPT_TRIANGLE_DEFAULT),
         "Max triangle size (" OPT_RECONSTRUCTION_POISSON ")")

        (OPT_SMOOTHER "," OPT_SMOOTHER_SHORT,
         bpo::value<std::size_t>(&opt_smoother)->
         default_value(OPT_SMOOTHER_DEFAULT),
         "Increases the scale by a number of iterations "
         "(" OPT_RECONSTRUCTION_SCALE_SPACE ")")

        (OPT_LENGTH "," OPT_LENGTH_SHORT,
         bpo::value<FT>(&opt_length)->
         default_value(OPT_LENGTH_DEFAULT),
         "Upper bound on the length of the facets "
         "(" OPT_RECONSTRUCTION_SCALE_SPACE ")")

        (OPT_EPSILON "," OPT_EPSILON_SHORT,
         bpo::value<FT>(&opt_epsilon)->
         default_value(OPT_EPSILON_DEFAULT),
         "Maximum tolerance distance from point and shape "
         "(" OPT_RECONSTRUCTION_FRONT_SURFACE ")")

        (OPT_CLUSTER "," OPT_CLUSTER_SHORT,
         bpo::value<FT>(&opt_cluster)->
         default_value(OPT_CLUSTER_DEFAULT, STR(OPT_CLUSTER_DEFAULT)),
         "Maximum distance between points to connected "
         "(" OPT_RECONSTRUCTION_FRONT_SURFACE ")")

        (OPT_POINTS "," OPT_POINTS_SHORT,
         bpo::value<FT>(&opt_points)->
         default_value(OPT_POINTS_DEFAULT),
         "Minimum number of points of a shape "
         "(" OPT_RECONSTRUCTION_FRONT_SURFACE ")")

        (OPT_PROBABILITY "," OPT_PROBABILITY_SHORT,
         bpo::value<FT>(&opt_probability)->
         default_value(OPT_PROBABILITY_DEFAULT, STR(OPT_PROBABILITY_DEFAULT)),
         "Probability to control search endurance "
         "(" OPT_RECONSTRUCTION_FRONT_SURFACE ")")

        (OPT_THRESHOLD "," OPT_THRESHOLD_SHORT,
         bpo::value<FT>(&opt_threshold)->
         default_value(OPT_THRESHOLD_DEFAULT, STR(OPT_THRESHOLD_DEFAULT)),
         "Maximum tolerance normal deviation "
         "(" OPT_RECONSTRUCTION_FRONT_SURFACE ")")

        (OPT_BINARY "," OPT_BINARY_SHORT,
         bpo::bool_switch(&opt_binary)->
         default_value(OPT_BINARY_DEFAULT),
         "Binary output (PLY)")

        (OPT_CONFIG "," OPT_CONFIG_SHORT,
         bpo::value<std::string>(),
         "Read config file")
        ;

        bpo::options_description desc_hidden("Hidden options");
        desc_hidden.add_options()
        (OPT_INPUT_FILE, bpo::value<std::string>(),
         "input file")

        (OPT_OUTPUT_FILE, bpo::value<std::string>(),
         "output file")
        ;

        bpo::positional_options_description pod;
        pod.add(OPT_INPUT_FILE, 1)
           .add(OPT_OUTPUT_FILE, -1);

        bpo::options_description cmdline_options;
        cmdline_options.add(desc)
                       .add(desc_hidden);

        bpo::variables_map vm;
        bpo::store(bpo::command_line_parser(argc, argv)
            .options(cmdline_options)
            .positional(pod)
            .run(), vm);

        // verbose

        const bool opt_verbose = vm.count(OPT_VERBOSE) > 0;

        // config file

        if (vm.count(OPT_CONFIG)) {
            const bfs::path config_file(vm[OPT_CONFIG].as<std::string>());

            if (opt_verbose) {
                std::cout << "Config file: " << config_file << std::endl;
            }

            bfs::ifstream fcfg(config_file);

            if (!fcfg) {
                THROW_SYSTEM_ERROR("failed to read config file: ", config_file);
            }

            store(parse_config_file(fcfg, cmdline_options), vm);
        }

        bpo::notify(vm);

        // help

        if (vm.count(OPT_HELP)) {
            std::cout << desc << std::endl;
            exit(EXIT_SUCCESS);
        }

        // version

        if (vm.count(OPT_VERSION)) {
            std::cout << "Version V" VERSION "\n"
			 "CGAL V" STR(CGAL_VERSION) "\n"
			 "Boost V" BOOST_LIB_VERSION
		      << std::endl;
            exit(EXIT_SUCCESS);
        }

        // orient normals

        const bool task_orient = vm.count(OPT_ORIENT) > 0;

        // input and output files

        const bfs::path input_file(vm.count(OPT_INPUT_FILE) ?
                                   vm[OPT_INPUT_FILE].as<std::string>() : "");

        const bfs::path output_file(vm.count(OPT_OUTPUT_FILE) ?
                                    vm[OPT_OUTPUT_FILE].as<std::string>() : "");

        if (input_file.empty() || output_file.empty()) {
            std::cerr << desc << std::endl;
            exit(EXIT_FAILURE);
        }

        if (opt_verbose) {
            std::cout << "Input file:  " << input_file << std::endl
                      << "Output file: " << output_file << std::endl;
        }

        // files extension

        const std::string input_ext = input_file.extension().string();
        const std::string output_ext = output_file.extension().string();

        // XYZ file
        if (boost::iequals(input_ext, EXT_XYZ) ||
            boost::iequals(input_ext, EXT_PWN)) {

            task_read_xyz = true;
        }
        // OFF file
        else if (boost::iequals(input_ext, EXT_OFF)) {

            task_read_off = true;
        }
        // PLY file
        else if (boost::iequals(input_ext, EXT_PLY)) {

            task_read_ply = true;
        }
        // LAS file
#ifdef CGAL_LINKED_WITH_LASLIB
        else if (boost::iequals(input_ext, EXT_LAS)) {

            task_read_las = true;
        }
#endif
        else {

            THROW_ERROR(
                (boost::format("Error unknown input file type: \"%1%\"")
                 % input_ext).str());
        }

        // XYZ or PWN file
        if (boost::iequals(output_ext, EXT_XYZ) ||
            boost::iequals(output_ext, EXT_PWN)) {

            task_write_xyz = true;
        }
        // OFF file
        else if (boost::iequals(output_ext, EXT_OFF)) {

            task_write_off = true;
        }
        // PLY file
        else if (boost::iequals(output_ext, EXT_PLY)) {

            task_write_ply = true;
        }
        // OBJ file
        else if (boost::iequals(output_ext, EXT_OBJ)) {

            task_write_obj = true;
        }
        // STL file
        else if (boost::iequals(output_ext, EXT_STL)) {

            task_write_stl = true;
        }
        // GOCAD file
        else if (boost::iequals(output_ext, EXT_TS)) {

            task_write_ts = true;
        }
        // LAS file
#ifdef CGAL_LINKED_WITH_LASLIB
        else if (boost::iequals(output_ext, EXT_LAS)) {

            task_write_las = true;
        }
#endif
        else {

            THROW_ERROR(
                (boost::format("Error unknown output file type: \"%1%\"")
                 % output_ext).str());
        }

        if (opt_verbose) {

            timer.start();
        }

        // Estimates normals direction

        // none
        if (opt_estimate == OPT_ESTIMATE_NONE) {

            // nothing
        }
        // plane
        else if (opt_estimate == OPT_ESTIMATE_PLANE) {

            task_estimate_plane = true;
        }
        // quadric
        else if (opt_estimate == OPT_ESTIMATE_QUADRIC) {

            task_estimate_quadric = true;
        }
        // vcm
        else if (opt_estimate == OPT_ESTIMATE_VCM) {

            task_estimate_vcm = true;
        }
        else {

            THROW_ERROR((boost::format(
                "Error unknown normal estimate:: \"%1%\"")
                    % opt_estimate).str());
        }

        // Reconstruction

        // none
        if (opt_reconstruction == OPT_RECONSTRUCTION_NONE) {

            // nothing
        }
        // convex-hull
        else if (opt_reconstruction == OPT_RECONSTRUCTION_CONVEX_HULL) {

            task_convex_hull = true;
        }
        // oriented-bounding-box
        else if (opt_reconstruction ==
                 OPT_RECONSTRUCTION_OPTIMAL_BOUNDING_BOX) {

            task_optimal_bounding_box = true;
        }
        // poisson
        else if (opt_reconstruction == OPT_RECONSTRUCTION_POISSON) {

            task_poisson = true;
        }
        // scale-space
        else if (opt_reconstruction == OPT_RECONSTRUCTION_SCALE_SPACE) {

            task_scale_space = true;
        }
        // front-surface
        else if (opt_reconstruction == OPT_RECONSTRUCTION_FRONT_SURFACE) {

            task_front_surface = true;
        }
        // polygonal-surface-reconstruction
        else if (opt_reconstruction ==
                 OPT_RECONSTRUCTION_POLYGONAL_SURFACE_RECONSTRUCTION) {

            task_polygonal_surface_reconstruction = true;
        }
        else {

            THROW_ERROR(
                (boost::format("Error: unknown surface reconstruction method: \"%1%\"")
                    % opt_reconstruction).str());
        }

        // read XYZ or OFF input file
        if (task_read_xyz || task_read_off) {

            if (!CGAL::IO::read_points(
                    input_file.string(),
                    std::back_inserter(points),
                    CGAL::parameters::point_map(Point_map())
                    .normal_map(Normal_map()))) {

                THROW_SYSTEM_ERROR(
                    "failed to read file: ",
                    input_file);
            }
        }

        // read PLY input file
        if (task_read_ply) {

            bfs::ifstream fin(input_file);

            if (!fin || !CGAL::IO::read_PLY_with_properties(
                    fin,
                    std::back_inserter(points),
                    CGAL::make_ply_point_reader(Point_map()),
                    CGAL::IO::make_ply_normal_reader(Normal_map()))) {

                THROW_SYSTEM_ERROR(
                    "failed to read PLY file: ",
                    input_file);
            }
        }

        if (points.empty()) {

            THROW_ERROR("Error the number of points is zero");
        }

        // Estimates normals direction

        // plane
        if (task_estimate_plane) {

            CGAL::pca_estimate_normals<CGAL::Parallel_if_available_tag>(
                points,
                opt_neighbors,
                CGAL::parameters::point_map(Point_map())
                    .normal_map(Normal_map()));
        }

        // quadric
        if (task_estimate_quadric) {

            CGAL::jet_estimate_normals<CGAL::Parallel_if_available_tag>(
                points,
                opt_neighbors,
                CGAL::parameters::point_map(Point_map())
                                 .normal_map(Normal_map()));
        }

        // vcm (Voronoi Covariance Measure)
        if (task_estimate_vcm) {

            CGAL::vcm_estimate_normals(
                points,
                opt_offset_radius,
                opt_convolution_radius,
                CGAL::parameters::point_map(Point_map())
                                 .normal_map(Normal_map()));
        }

        if (opt_verbose) {

            std::cout << "Estimate normals: "
                      << bio::quoted(opt_estimate)
                      << std::endl;
        }

        // Orients normals

        if (task_orient) {

            const Point_vec::iterator unoriented_points_begin =
                CGAL::mst_orient_normals(
                    points,
                    opt_neighbors_mst,
                    CGAL::parameters::normal_map(
                        CGAL::make_normal_of_point_with_normal_map(
                            Point_vec::value_type())));

            // delete points with an unoriented normal
            points.erase(unoriented_points_begin, points.end());

            if (opt_verbose) {

                std::cout << "Orients the normals using a Minimum Spanning Tree"
                          << std::endl;
            }
        }

        if (points.size() < 3) {

            THROW_ERROR("Error the number of points is less than 3");
        }

        if (opt_verbose) {

            const auto size = points.size();

            std::cout << "Total points: " << size << std::endl;

#ifdef CFG_DUMP
            if (size > 0) {
                std::cout << "| Point | Normal |" << std::endl;

                BOOST_FOREACH(const Point_with_normal &pnt, points) {

                    std::cout << "| ("
                              << pnt.position()
                              << ") | ("
                              << pnt.normal()
                              << ") |"
                              << std::endl;
                }
            }
#endif
        }

        // Convex Hulls reconstruction

        if (task_convex_hull) {

            CGAL::convex_hull_3(
                points.begin(),
                points.end(),
                output_mesh);

            if (opt_verbose) {

                std::cout << "Convex Hulls reconstruction faces: "
                          << output_mesh.size_of_facets()
                          << std::endl;
            }
        }

        // Make a hexahedron out of the oriented bounding box

        if (task_optimal_bounding_box) {

            Surface_mesh sm;
            std::array<Point, 8> obb_points;

            BOOST_FOREACH(const Point_with_normal &pnt, points) {
                sm.add_vertex(pnt.position());
            }

            CGAL::oriented_bounding_box(
                sm,
                obb_points,
                CGAL::parameters::use_convex_hull(true));

            CGAL::make_hexahedron(
                obb_points[0], obb_points[1],
                obb_points[2], obb_points[3],
                obb_points[4], obb_points[5],
                obb_points[6], obb_points[7],
                output_mesh);

            if (opt_verbose) {

                std::cout << "Optimal Bounding Box faces: "
                          << output_mesh.size_of_facets()
                          << std::endl;
            }
        }

        // Poisson Delaunay reconstruction

        if (task_poisson) {

            const bool test_normals = (points.begin()->normal() != CGAL::NULL_VECTOR);

            if (!test_normals) {

                THROW_ERROR("Error: reconstruction method requires oriented normals");
            }

            STr tr;             // 3D Delaunay triangulation for surface mesh generation
            C2t3 c2t3(tr);      // 2D complex in 3D Delaunay triangulation

            // Surface approximation error w.r.t. point set average spacing.
            const auto sm_distance = opt_distance;

            // Min triangle angle in degrees.
            const auto sm_angle = opt_angle;

            // Max triangle size w.r.t. point set average spacing.
            const auto sm_radius = opt_triangle;

            // Creates implicit function from the read points
            Poisson_reconstruction_function function(
                points.begin(),
                points.end(),
                CGAL::make_normal_of_point_with_normal_map(
                    Point_vec::value_type()));

            if (!function.compute_implicit_function()) {

                THROW_ERROR("Error Poisson Delaunay reconstruction");
            }

            // Computes average spacing
            FT average_spacing = CGAL::compute_average_spacing<CGAL::Parallel_if_available_tag>(
                points,
                opt_neighbors_avg);

            // Gets one point inside the implicit surface
            // and computes implicit function bounding sphere radius.
            const Point inner_point = function.get_inner_point();
            const FT inner_point_value = function(inner_point);

            if (inner_point_value >= 0.0) {

                THROW_ERROR(
                    (boost::format("Error: unable to seed %1% at inner_point")
                        % inner_point_value).str());
            }

            // Gets implicit function's radius
            const Sphere bsphere = function.bounding_sphere();
            const FT radius = std::sqrt(bsphere.squared_radius());

            // Defines the implicit surface: requires defining a
            // conservative bounding sphere centered at inner point.
            const FT sm_sphere_radius = 5.0 * radius;

            // Dichotomy error must be << sm_distance
            const FT sm_dichotomy_error =
                sm_distance * average_spacing / 1000.0;

            Surface_3 surface(
                function,
                Sphere(inner_point, sm_sphere_radius * sm_sphere_radius),
                sm_dichotomy_error / sm_sphere_radius);

            // Defines surface mesh generation criteria
            CGAL::Surface_mesh_default_criteria_3<STr> criteria(
                sm_angle,                           // Min triangle angle (degrees)
                sm_radius * average_spacing,        // Max triangle size
                sm_distance * average_spacing);     // Approximation error

            // Generates surface mesh with manifold option
            CGAL::make_surface_mesh(
                c2t3,                                 // reconstructed mesh
                surface,                              // implicit surface
                criteria,                             // meshing criteria
                CGAL::Manifold_with_boundary_tag());  // require manifold mesh

            if (tr.number_of_vertices() == 0) {

                THROW_ERROR("Error mesh null");
            }

            CGAL::facets_in_complex_2_to_triangle_mesh(
                c2t3,
                output_mesh);

            if (opt_verbose) {

                std::cout << "Poisson Delaunay reconstruction faces: "
                          << c2t3.number_of_facets()
                          << std::endl;
            }
        }

        // Scale-Space Surface reconstruction

        if (task_scale_space) {

            Reconstruction reconstruct(
                points.begin(),
                points.end());

            reconstruct.increase_scale<Smoother>(opt_smoother);

            Mesher mesher(opt_length);
            reconstruct.reconstruct_surface(mesher);

            if (reconstruct.number_of_facets() == 0) {
                THROW_ERROR("Error: mesh null");
            }

            BuilderPoisson<HalfedgeDS> builder(reconstruct);
            output_mesh.delegate(builder);

            if (opt_verbose) {
                std::cout << "Scale-Space Surface reconstruction faces: "
                          << reconstruct.number_of_facets()
                          << std::endl;
            }
        }

        // Advancing Front Surface reconstruction

        if (task_front_surface) {

            Efficient_ransac ransac;
            ransac.set_input(points);

            // Only planes are useful for stucturing
            ransac.add_shape_factory<Plane>();

            // Default RANSAC parameters
            Efficient_ransac::Parameters op;

            op.probability = opt_probability;
            op.min_points = opt_points;
            op.epsilon = opt_epsilon;
            op.cluster_epsilon = opt_cluster;
            op.normal_threshold = opt_threshold;

            ransac.detect(op); // Plane detection
            Efficient_ransac::Plane_range planes = ransac.planes();

            Structure pss(
                points,
                planes,
                op.cluster_epsilon,  // Same parameter as RANSAC
                CGAL::parameters::point_map(Point_map())
                                 .normal_map(Normal_map())
                                 .plane_map(CGAL::Shape_detection::Plane_map<Traits>())
                                 .plane_index_map(
                                    CGAL::Shape_detection::Point_to_shape_index_map<Traits>(
                                        points, planes)));

            auto pair_point_index = [&pss](const std::size_t index) {
                return std::make_pair(pss[index].first, index);
            };

            Triangulation_3 triang(
                boost::make_transform_iterator(
                    boost::counting_iterator<std::size_t>(0),
                    pair_point_index),
                boost::make_transform_iterator(
                    boost::counting_iterator<std::size_t>(pss.size()),
                    pair_point_index));

            PriorityStructureCoherence<Structure> priority(
                pss,
                1000. * op.cluster_epsilon); // Avoid too large facets

            ReconstructionAFS reconstruct(triang, priority);
            reconstruct.run();

            BuilderAFS<HalfedgeDS> builder(pss, reconstruct);
            output_mesh.delegate(builder);

            if (opt_verbose) {

                std::cout << "Advancing Front Surface reconstruction faces: "
                          << reconstruct.number_of_facets()
                          << std::endl;
            }
        }

        // Polygonal Surface Reconstruction

        if (task_polygonal_surface_reconstruction) {

            // Shape detection
            Efficient_ransac ransac;
            ransac.set_input(points);
            ransac.add_shape_factory<Plane>();
            ransac.detect();
            Efficient_ransac::Plane_range planes = ransac.planes();

            Point_index_vec points_index(points.size());
            Point_to_shape_index_map shape_index_map(points, planes);

            for (auto i = 0; i < points.size(); ++i) {

                const auto &pnt = points[i];

                points_index[i] = Point_with_normal_index(
                        pnt.position(),
                        pnt.normal(),
                        get(shape_index_map, i));
            }

            Polygonal_surface_reconstruction algo(
                points_index,
                Point_index_map(),
                Normal_index_map(),
                Plane_index_map());

            if (!algo.reconstruct<MIP_Solver>(output_mesh)) {

                THROW_ERROR(
                    (boost::format("Error MIP solver: %1%")
                        % algo.error_message()).str());
            }

            if (opt_verbose) {

                std::cout << "Polygonal Surface Reconstruction faces: "
                          << output_mesh.size_of_facets()
                          << " extracting planes: "
                          << planes.size()
                          << std::endl;
            }
        }

        // output file

        const auto facets = output_mesh.size_of_facets();

        if ((facets > 0) and !task_write_xyz) {

            // write OBJ,PLY,OFF,STL,GOCAD output file (mesh)
            if (task_write_obj ||
                task_write_ply ||
                task_write_off ||
                task_write_stl ||
                task_write_ts) {

                if (!CGAL::IO::write_polygon_mesh(
                        output_file.string(),
                        output_mesh,
                        CGAL::parameters::use_binary_mode(opt_binary))) {

                    THROW_SYSTEM_ERROR(
                        "failed to write file: ",
                        output_file);
                }

                write_file = true;
            }
        }
        else {

            // write XYZ,OFF,PLY output file only points
            if (task_write_xyz || task_write_off || task_write_ply) {

                if (!CGAL::IO::write_points(
                        output_file.string(),
                        points,
                        CGAL::parameters::point_map(Point_map())
                        .normal_map(Normal_map())
                        .use_binary_mode(opt_binary))) {

                    THROW_SYSTEM_ERROR(
                        "failed to write file: ", output_file);
                }

                write_file = true;
            }

            // write OBJ output file (points)
            if (task_write_obj) {

                // create a fake mesh
                BuilderPoints<HalfedgeDS> builder(points);
                output_mesh.delegate(builder);

                if (!CGAL::IO::write_OBJ(
                        output_file.string(),
                        output_mesh)) {

                    THROW_SYSTEM_ERROR(
                        "failed to write OBJ file: ", output_file);
                }

                write_file = true;
            }
        }

        if (!write_file) {

            THROW_ERROR("Error not write file");
        }

        if (opt_verbose) {

            timer.stop();

            if (facets > 0) {

                if (!CGAL::is_triangle_mesh(output_mesh)) {

                    CGAL::Polygon_mesh_processing::triangulate_faces(output_mesh);
                }

                std::cout << "Volume: "
                          << CGAL::Polygon_mesh_processing::volume(output_mesh)
                          << std::endl;
            }

            const bun::quantity<byte_base_unit::unit_type> memory =
                    CGAL::Memory_sizer().virtual_size() *
                    byte_base_unit::unit_type();

            std::cout << "Memory allocated: "
                      << bun::binary_prefix << memory << bun::no_prefix
                      << std::endl
                      << "Elapsed time: " << timer.time() << " s"
                      << std::endl;
        }

    } catch (const bpo::error &ex) {

        std::cerr << ex.what() << std::endl;
        exit(EXIT_FAILURE);

    } catch (const bfs::filesystem_error &ex) {

        std::cerr << ex.what() << std::endl;
        exit(EXIT_FAILURE);

    } catch (const std::system_error &ex) {

        std::cerr << ex.what() << " (" << ex.code() << ')' << std::endl;
        exit(EXIT_FAILURE);

    } catch (const std::runtime_error &ex) {

        std::cerr << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}
